/** Connect/Switch to new DB **/
\c ro;

/** Restrict on creating table in public schema **/
REVOKE ALL ON SCHEMA public FROM PUBLIC;

/** Create Schema **/
CREATE SCHEMA IF NOT EXISTS ro_owner;

/** Create role ro_impl_role **/
DO $$
BEGIN
 CREATE ROLE ro_impl_role WITH
        NOLOGIN
        NOSUPERUSER
        INHERIT
        NOCREATEDB
        NOCREATEROLE
        NOREPLICATION;
        EXCEPTION WHEN DUPLICATE_OBJECT THEN
        RAISE NOTICE 'Role ro_impl_role already exists';
END
$$;

/** Create User with login **/
DO $$
BEGIN
CREATE ROLE ro_user WITH
	LOGIN
	NOSUPERUSER
	NOCREATEDB
	NOCREATEROLE
	INHERIT
	NOREPLICATION
	CONNECTION LIMIT -1
	PASSWORD 'rouser123';
  EXCEPTION WHEN DUPLICATE_OBJECT THEN
  RAISE NOTICE 'User ro_user already exists';
END
$$;

/** Grant Create table access for ro_owner **/
GRANT USAGE, CREATE ON SCHEMA ro_owner TO ro_impl_role;

/** Table Level Access **/
GRANT SELECT, INSERT, UPDATE ON ALL TABLES IN SCHEMA ro_owner TO ro_impl_role;

/** Automatically grant permissions on tables and views added **/
ALTER DEFAULT PRIVILEGES IN SCHEMA ro_owner GRANT SELECT, INSERT, UPDATE ON TABLES TO ro_impl_role;

/** Grant permission to all sequences **/
GRANT USAGE ON ALL SEQUENCES IN SCHEMA ro_owner TO ro_impl_role;

/** Grant permissions to sequences added in the future**/
ALTER DEFAULT PRIVILEGES IN SCHEMA ro_owner GRANT USAGE ON SEQUENCES TO ro_impl_role;

/** Grant User with Role **/
GRANT ro_impl_role to ro_user;

/** Grant DATABASE with login **/
GRANT CONNECT on DATABASE ro to ro_user;

CREATE EXTENSION IF NOT EXISTS pg_trgm SCHEMA pg_catalog VERSION "1.4";
