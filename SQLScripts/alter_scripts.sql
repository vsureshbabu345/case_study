ALTER TABLE ro_owner.recognising_organisation ADD COLUMN crm_system VARCHAR(50);
ALTER TABLE ro_owner.contact ADD COLUMN title VARCHAR(20), ADD COLUMN job_title VARCHAR(100);
ALTER TABLE ro_owner.recognising_organisation ALTER COLUMN website_url TYPE varchar(250);
