CREATE OR REPLACE FUNCTION ro_owner.ro_fuzzy_search(_org_name text, _city_name text, _postal_code text) 
RETURNS TABLE (recognising_organisation_uuid uuid, organisation_type_uuid uuid, sector_type_uuid uuid, 
			   organisation_id integer, name character varying, verification_status ro_owner.verification_status, 
			   partner_code character varying, partner_contact character varying, method_of_delivery ro_owner.method_of_delivery,
			   org_status ro_owner.org_status, website_url character varying, 
			   organisation_code character varying, soft_deleted boolean, 
			   address_uuid uuid, address_type_uuid uuid, contact_uuid uuid, territory_uuid uuid, country_uuid uuid, 
			   addressline1 character varying, addressline2 character varying, addressline3 character varying, 
			   addressline4 character varying, city character varying, 
			   postalcode character varying, email character varying, phone character varying, target_recognising_organisation_uuid uuid,
			   sim_score real)
LANGUAGE plpgsql
AS $function$
DECLARE postalCodeQuery varchar DEFAULT ''; 
DECLARE mainAddressTypeQuery varchar DEFAULT ''; 
begin
mainAddressTypeQuery := ' AND address_type_uuid = (SELECT address_type_uuid FROM ro_owner.address_type WHERE address_type_name =''Main'')';
-- Null or empty check for city
if  _city_name  <> '' then 
postalCodeQuery := ' AND ( $3 is NULL OR $3 = '''' OR (lower(postalcode) = lower($3) AND contact_uuid is NULL ' || mainAddressTypeQuery || ')) ORDER BY sim_score desc;'; 
else
-- Null or empty check for org name
if _org_name  <> '' then
postalCodeQuery := ' AND ($3 is NULL OR $3 = '''' OR (lower(postalcode) = lower($3) AND contact_uuid is NULL ' || mainAddressTypeQuery || ')) ORDER BY sim_score desc;'; 
else 
postalCodeQuery := ' OR ($3 is NULL OR $3 = '''' OR (lower(postalcode) = lower($3) AND contact_uuid is NULL ' || mainAddressTypeQuery || ')) ORDER BY sim_score desc;';
end if;
end if; 
RETURN QUERY EXECUTE
'SELECT distinct o.recognising_organisation_uuid, organisation_type_uuid, sector_type_uuid, organisation_id, name,
verification_status, partner_code, partner_contact, method_of_delivery, org_status,
website_url, organisation_code , soft_deleted, 
address_uuid, address_type_uuid, contact_uuid, territory_uuid, country_uuid, addressline1, addressline2, 
addressline3, addressline4, city, postalcode, email, phone, target_recognising_organisation_uuid, 
similarity(name, $1) as sim_score
FROM (ro_owner.recognising_organisation o LEFT OUTER JOIN ro_owner.address a
	on o.recognising_organisation_uuid = a.recognising_organisation_uuid 
    LEFT OUTER JOIN ro_owner.linked_recognising_organisation lro
	on o.recognising_organisation_uuid = lro.source_recognising_organisation_uuid AND lro.linked_recognising_organisation_type = ''PARENT_RO'') 
WHERE 
( $1 IS NULL OR $1 = '''' OR  name % $1 )
AND similarity(name,$1 ) >= 0.5
AND contact_uuid is NULL
AND soft_deleted = false 
UNION 
SELECT distinct o.recognising_organisation_uuid, organisation_type_uuid, sector_type_uuid, organisation_id, name,
verification_status, partner_code,  partner_contact, method_of_delivery, org_status,
website_url, organisation_code , soft_deleted, 
address_uuid, address_type_uuid, contact_uuid, territory_uuid, country_uuid, addressline1, addressline2, 
addressline3, addressline4, city, postalcode, email, phone, target_recognising_organisation_uuid, 
similarity(city, $2) as sim_score
FROM (ro_owner.recognising_organisation o LEFT OUTER JOIN ro_owner.address a
	on o.recognising_organisation_uuid = a.recognising_organisation_uuid
 LEFT OUTER JOIN ro_owner.linked_recognising_organisation lro
	on o.recognising_organisation_uuid = lro.source_recognising_organisation_uuid AND lro.linked_recognising_organisation_type = ''PARENT_RO'') 
WHERE 
(( $2  IS NULL OR (city %  $2 AND contact_uuid is NULL ' || mainAddressTypeQuery || ')) AND similarity(city,$2 ) >=0.5)
AND soft_deleted = false '  ||  postalCodeQuery 
USING  _org_name,_city_name,_postal_code; 
end;
$function$;
