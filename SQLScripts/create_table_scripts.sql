/** Type For Verification Status **/
DO $$ BEGIN
	CREATE TYPE ro_owner.verification_status AS ENUM
    ('PENDING','APPROVED','VERIFIED','REJECTED');
	EXCEPTION
    WHEN duplicate_object THEN null;
END $$;

/** Type For Method Of Delivery Type **/
DO $$ BEGIN
	CREATE TYPE ro_owner.method_Of_delivery AS ENUM
    ('POSTAL','E-DELIVERY');
	EXCEPTION
    WHEN duplicate_object THEN null;
END $$;

/** Type For Organisation Status **/
DO $$ BEGIN
	CREATE TYPE ro_owner.org_status AS ENUM ('INACTIVE','ACTIVE');
	EXCEPTION
    WHEN duplicate_object THEN null;
END $$;

/** Create table for recognising_organisation **/
CREATE TABLE IF NOT EXISTS ro_owner.recognising_organisation( 
	recognising_organisation_uuid UUID NOT NULL,
	organisation_type_uuid UUID NOT NULL,
    sector_type_uuid UUID NOT NULL,
    organisation_id INT NOT NULL,
    name VARCHAR(255) NOT NULL,
    verification_status ro_owner.verification_status NOT NULL,
    partner_code VARCHAR(25) NOT NULL,
	partner_contact VARCHAR(50),
    method_of_delivery ro_owner.method_Of_delivery NOT NULL,
    parent_recognising_organisation_uuid UUID,
    org_status ro_owner.org_status NOT NULL,
    website_url VARCHAR(250),
	crm_system VARCHAR(50),
    organisation_code VARCHAR(20),
    replaced_by_recognising_organisation_uuid UUID DEFAULT NULL,
    soft_deleted Boolean DEFAULT FALSE,
    concurrency_version INTEGER NOT NULL,
    created_by VARCHAR(36) NOT NULL,
	created_datetime timestamptz NOT NULL,
	updated_by VARCHAR(36) NULL,
	updated_datetime timestamptz NULL,
    CONSTRAINT pk_recognising_organisation PRIMARY KEY (recognising_organisation_uuid)
	);
	
CREATE SEQUENCE IF NOT EXISTS ro_owner.organisationId_generator AS INT 
INCREMENT BY 1  
MAXVALUE 999999  
START WITH 171183 
OWNED BY ro_owner.recognising_organisation.organisation_id;	
	
/*** Creating table for contact**/	
CREATE TABLE IF NOT EXISTS ro_owner.contact(
    contact_uuid UUID NOT NULL,
    recognising_organisation_uuid  UUID NOT NULL,
    contact_type_uuid UUID NOT NULL,
	title VARCHAR(20),
    first_name VARCHAR(50),
	last_name VARCHAR(50),
	job_title VARCHAR(100),
    effective_from_datetime timestamptz NOT NULL,
    effective_to_datetime timestamptz NOT NULL,
    concurrency_version INTEGER NOT NULL,
    created_by VARCHAR(36) NOT NULL,
	created_datetime timestamptz NOT NULL,
	updated_by VARCHAR(36),
	updated_datetime timestamptz,
    CONSTRAINT pk_contact PRIMARY KEY (contact_uuid),
    CONSTRAINT fk_01_contact_recognising_org FOREIGN KEY (recognising_organisation_uuid ) REFERENCES ro_owner.recognising_organisation (recognising_organisation_uuid) ON UPDATE NO ACTION ON DELETE NO ACTION
	);	 


/*** Creating table to store Address**/	
CREATE TABLE IF NOT EXISTS ro_owner.address( 
    address_uuid UUID NOT NULL,
    recognising_organisation_uuid UUID,
    address_type_uuid UUID NOT NULL,
    contact_uuid UUID,
    territory_uuid UUID,
    country_uuid UUID,
    addressline1 VARCHAR(100),
    addressline2 VARCHAR(100),
    addressline3 VARCHAR(100),
    addressline4 VARCHAR(100),
    city VARCHAR(100),
    postalcode VARCHAR(20),
    email VARCHAR(320) DEFAULT NULL,
    phone VARCHAR(20),
    concurrency_version INTEGER NOT NULL,
    created_by VARCHAR(36) NOT NULL,
	created_datetime timestamptz NOT NULL,
	updated_by VARCHAR(36),
	updated_datetime timestamptz,
    CONSTRAINT pk_address PRIMARY KEY (address_uuid),
    CONSTRAINT fk_01_address_recognising_org FOREIGN KEY (recognising_organisation_uuid ) REFERENCES ro_owner.recognising_organisation (recognising_organisation_uuid) ON UPDATE NO ACTION ON DELETE NO ACTION,
    CONSTRAINT fk_02_address_contact FOREIGN KEY (contact_uuid) REFERENCES ro_owner.contact (contact_uuid) ON UPDATE NO ACTION ON DELETE NO ACTION	
	);
       

/*** Creating table for ro_note**/	
CREATE TABLE IF NOT EXISTS ro_owner.ro_note( 
    note_uuid UUID NOT NULL,
    recognising_organisation_uuid  UUID NOT NULL,
    note_type_uuid UUID NOT NULL,
    note_content VARCHAR(2000),
    concurrency_version INTEGER NOT NULL,
    created_by VARCHAR(36) NOT NULL,
	created_datetime timestamptz NOT NULL,
	updated_by VARCHAR(36),
	updated_datetime timestamptz,
    CONSTRAINT pk_notes PRIMARY KEY (note_uuid),
    CONSTRAINT fk_01_ro_note_recognising_org FOREIGN KEY (recognising_organisation_uuid ) REFERENCES ro_owner.recognising_organisation (recognising_organisation_uuid) ON UPDATE NO ACTION ON DELETE NO ACTION
	);

/*** Creating table for alternate_name**/	
CREATE TABLE IF NOT EXISTS ro_owner.alternate_name(
    alternate_name_uuid UUID NOT NULL,
    recognising_organisation_uuid  UUID NOT NULL,
    name VARCHAR(255) NOT NULL,
    concurrency_version INTEGER NOT NULL,
    created_by VARCHAR(36) NOT NULL,
	created_datetime timestamptz NOT NULL,
	updated_by VARCHAR(36),
	updated_datetime timestamptz,
    CONSTRAINT pk_alternate_name PRIMARY KEY (alternate_name_uuid),
    CONSTRAINT fk_01_alternate_name_recognising_org FOREIGN KEY (recognising_organisation_uuid ) REFERENCES ro_owner.recognising_organisation (recognising_organisation_uuid) ON UPDATE NO ACTION ON DELETE NO ACTION
	);
	
/** Type For Component Type **/
DO $$ BEGIN
	CREATE TYPE ro_owner.component_type AS ENUM ('R','L','W','S','ALL');
	EXCEPTION
    WHEN duplicate_object THEN null;
END $$;	

/*** Creating table for minimum_score**/	
CREATE TABLE IF NOT EXISTS ro_owner.minimum_score(
    minscore_req_uuid UUID NOT NULL,
    recognising_organisation_uuid  UUID NOT NULL,
    module_type_uuid UUID NOT NULL,
    component ro_owner.component_type NOT NULL,
    minimum_score_value real NOT NULL,
    concurrency_version INTEGER NOT NULL,
    created_by VARCHAR(36) NOT NULL,
	created_datetime timestamptz NOT NULL,
	updated_by VARCHAR(36),
	updated_datetime timestamptz,
    CONSTRAINT pk_minimum_score PRIMARY KEY (minscore_req_uuid),
    CONSTRAINT fk_01_min_score_recognising_org FOREIGN KEY (recognising_organisation_uuid ) REFERENCES ro_owner.recognising_organisation (recognising_organisation_uuid) ON UPDATE NO ACTION ON DELETE NO ACTION
	);
	
/*** Creating table for recognised_product**/	
CREATE TABLE IF NOT EXISTS ro_owner.recognised_product( 
    recognised_product_uuid UUID NOT NULL,
    recognising_organisation_uuid  UUID NOT NULL,
    product_uuid UUID NOT NULL,
    effective_from_datetime timestamptz NOT NULL,
    effective_to_datetime timestamptz DEFAULT '2099-12-31 00:00:00+00',
    concurrency_version INTEGER NOT NULL,
    created_by VARCHAR(36) NOT NULL,
	created_datetime timestamptz NOT NULL,
	updated_by VARCHAR(36),
	updated_datetime timestamptz,
    CONSTRAINT pk_recognised_product PRIMARY KEY (recognised_product_uuid),
    CONSTRAINT fk_01_recognised_product_recognising_org FOREIGN KEY (recognising_organisation_uuid ) REFERENCES ro_owner.recognising_organisation (recognising_organisation_uuid) ON UPDATE NO ACTION ON DELETE NO ACTION
	);

/** Type For Status **/
DO $$ BEGIN
	CREATE TYPE ro_owner.status AS ENUM ('PENDING','APPROVED','VERIFIED','REJECTED','INACTIVE','ACTIVE');
	EXCEPTION
    WHEN duplicate_object THEN null;
END $$;

/** Type For Status Type**/
DO $$ BEGIN
	CREATE TYPE ro_owner.status_type AS ENUM ('ORGANISATION STATUS','VERIFICATION STATUS');
	EXCEPTION
    WHEN duplicate_object THEN null;
END $$;

/*** Creating table for status_history**/	
CREATE TABLE IF NOT EXISTS ro_owner.status_history( 
    status_history_uuid UUID NOT NULL,
    recognising_organisation_uuid  UUID NOT NULL,
    status ro_owner.status NOT NULL,
	status_type ro_owner.status_type NOT NULL,
    status_datetime timestamptz NOT NULL,
    concurrency_version INTEGER NOT NULL,
    created_by VARCHAR(36) NOT NULL,
	created_datetime timestamptz NOT NULL,
	updated_by VARCHAR(36),
	updated_datetime timestamptz,
    CONSTRAINT pk_status_history PRIMARY KEY (status_history_uuid ),
    CONSTRAINT fk_01_status_history_recognising_org FOREIGN KEY (recognising_organisation_uuid ) REFERENCES ro_owner.recognising_organisation (recognising_organisation_uuid) ON UPDATE NO ACTION ON DELETE NO ACTION
	);	 
