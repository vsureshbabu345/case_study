--Connect/Switch to new DB 
\c ro

-- IELTS Organisation Micoservice SQL scripts
\i create_schema.sql

--location DDL scripts
\ir  location//location_ddl//location_ddl.sql

-- Product DDL scripts
\ir  product//product_ddl//create_product.sql
-- Product DML scripts
\ir  product//product_dml//insert_product.sql

-- User_group_hierarchy DDL scripts
\ir  rbac//user_group_hierarchy_ddl.sql

-- User_group_hierarchy DML scripts
\ir  rbac//user_group_hierarchy_dml.sql

\ir  reference_ddl//create_address_type.sql
\ir  reference_ddl//create_contact_type.sql
\ir  reference_ddl//create_country.sql
\ir  reference_ddl//create_module_type.sql
\ir  reference_ddl//create_note_type.sql
\ir  reference_ddl//create_organisation_type.sql
\ir  reference_ddl//create_partner.sql
\ir  reference_ddl//create_sector_type.sql
\ir  reference_ddl//create_territory.sql

\ir  reference_dml//insert_address_type.sql
\ir  reference_dml//insert_contact_type.sql
\ir  reference_dml//insert_country.sql
\ir  reference_dml//insert_module_type.sql
\ir  reference_dml//insert_note_type.sql
\ir  reference_dml//insert_organisation_type.sql
\ir  reference_dml//insert_partner.sql
\ir  reference_dml//insert_sector_type.sql
\ir  reference_dml//insert_territory.sql

\i create_table_scripts.sql
\i duplicate_search.sql
